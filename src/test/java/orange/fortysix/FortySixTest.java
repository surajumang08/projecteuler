package orange.fortysix;

import static org.assertj.core.api.Assertions.*;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class FortySixTest {

    private FortySix fortySix = new FortySix();

    @Test
    public void isWhole() {
        assertThat(fortySix.isWhole(12.0)).isTrue();
        assertThat(fortySix.isWhole(12.2)).isFalse();
    }

    @Test
    public void isEven() {
        assertThat(fortySix.isEven(12)).isTrue();
        assertThat(fortySix.isEven(23)).isFalse();
    }

    @Test
    public void checkLoop() {
        assertThat(fortySix.loop(9)).isNegative();
        assertThat(fortySix.loop(21)).isNegative();
        assertThat(fortySix.loop(25)).isNegative();
        assertThat(fortySix.loop(27)).isNegative();
        assertThat(fortySix.loop(33)).isNegative();
    }

    @Test
    public void execute() {
        fortySix.execute(10000);
    }
}