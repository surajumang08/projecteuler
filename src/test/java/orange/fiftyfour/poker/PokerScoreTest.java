package orange.fiftyfour.poker;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public class PokerScoreTest {

    @Test
    public void getHighestScore() {
        Assertions.assertThat(PokerScore.getHighestScore(Pokerhand.simpleHand))
                .isEqualByComparingTo(Score.createScore(Rank.HIGH_CARD));
    }
}