package orange.fiftyfour.poker.calculator;

import orange.fiftyfour.poker.calculator.checker.ConsecutiveValuesChecker;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created 3/5/2019
 *
 * @author sjkumar
 */
public class ConsecutiveValuesCheckerTest {
    @Test
    public void simpleTest() {
        ConsecutiveValuesChecker cv = new ConsecutiveValuesChecker(2);
        assertThat(cv.check1("24356")).isTrue();
        assertThat(cv.check1("13579A")).isFalse();
        cv = new ConsecutiveValuesChecker(4);
        assertThat(cv.check1("TJQK")).isTrue();
        assertThat(cv.check1("TQKA")).isFalse();
        assertThat(cv.check1("56789")).isTrue();
    }
}