package orange.fiftyfour.poker.calculator;

import orange.fiftyfour.poker.Pokerhand;
import orange.fiftyfour.poker.Rank;
import orange.fiftyfour.poker.Score;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public class FlushCalculatorTest {
    private Pokerhand flushHand = new Pokerhand(Arrays.asList("2S", "3S", "4S", "5S", "6S"));

    @Test
    public void testCalculate() {
        FlushCalculator flushCalculator = new FlushCalculator(flushHand);
        assertThat(flushCalculator.calculate()).isEqualByComparingTo(Score.createScore(Rank.FLUSH, 6));
        assertThat(flushCalculator.calculate()).isGreaterThan(Score.createScore(Rank.ONE_PAIR));
    }

    @Test
    public void isFlush() {
        FlushCalculator flushCalculator = new FlushCalculator(flushHand);
        assertThat(flushCalculator.isFlush()).isTrue();
        assertThat(FlushCalculator.isFlush(Pokerhand.simpleHand)).isFalse();
    }
}