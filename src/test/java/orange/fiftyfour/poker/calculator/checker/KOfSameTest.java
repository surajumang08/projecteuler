package orange.fiftyfour.poker.calculator.checker;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created 3/5/2019
 *
 * @author sjkumar
 */
public class KOfSameTest {

    @Test
    public void simple() {
        KOfSame k = new KOfSame(3);
        assertThat(k.check("333333")).isTrue();
        assertThat(k.check("SSDDAA")).isFalse();
    }

    @Test
    public void simpleWithFive() {
        KOfSame k = new KOfSame(5);
        assertThat(k.check("333333")).isTrue();
        assertThat(k.check("SSSSS")).isTrue();
    }
}