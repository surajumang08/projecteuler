package orange.fiftyfour.poker;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created 3/5/2019
 *
 * @author sjkumar
 */
public class PokerhandTest {

    @Test
    public void cardOder_returnsTrue() {
        Pokerhand.Card ten = new Pokerhand.Card("T", "S");
        Pokerhand.Card four = new Pokerhand.Card("4", "S");
        Pokerhand.Card nine = new Pokerhand.Card("9", "C");
        Pokerhand.Card ace = new Pokerhand.Card("A", "S");
        Pokerhand.Card jack = new Pokerhand.Card("J", "H");
        Pokerhand.Card queen = new Pokerhand.Card("Q", "D");

        assertThat(ten.compareTo(four)).isPositive();
        assertThat(ace.compareTo(ten)).isPositive();
        assertThat(nine.compareTo(four)).isPositive();
        assertThat(jack.compareTo(queen)).isNegative();
    }

    @Test
    public void returnedInOrder() {
        Pokerhand pokerhand = new Pokerhand(Arrays.asList("AS", "QD", "2S", "TH", "9C"));
        assertThat(pokerhand.getHand().get(0)).isEqualByComparingTo(Pokerhand.Card.create("2S"));
        assertThat(pokerhand.getHand().get(3)).isEqualByComparingTo(Pokerhand.Card.create("QD"));
    }

    @Test
    public void testCardValueAsInt() {
        Pokerhand.Card queen = Pokerhand.Card.create("QS");
        Pokerhand.Card ace = Pokerhand.Card.create("AD");
        assertThat(ace.getCardValueAsInt()).isEqualTo(14);
        assertThat(queen.getCardValueAsInt()).isEqualTo(12);
    }
}