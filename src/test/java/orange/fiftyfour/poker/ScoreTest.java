package orange.fiftyfour.poker;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public class ScoreTest {

    @Test
    public void compareTo() {
        Score score = Score.createScore(Rank.HIGH_CARD, 1);
        Score score2 = Score.createScore(Rank.HIGH_CARD, 2);
        assertThat(score.compareTo(score2)).isNegative();

        Score score1 = Score.createScore(Rank.FLUSH);
        Score score3 = Score.createScore(Rank.FLUSH);

        assertThat(score3.compareTo(score1)).isZero();
        assertThat(score1.compareTo(score)).isPositive();
    }
}