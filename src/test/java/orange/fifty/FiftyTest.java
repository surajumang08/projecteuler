package orange.fifty;

import static org.assertj.core.api.Assertions.*;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class FiftyTest {

    @Test
    public void withLessThan1000(){
        Fifty fifty = new Fifty();
        fifty.execute();
    }

    @Test
    public void testPrimes(){
        Fifty fifty = new Fifty();
        fifty.addPrimes(4);
        Assertions.assertThat(Fifty.primes).contains(2,3,5,7);
    }

    @Test
    public void testMaxSequence(){
        Fifty fifty = new Fifty();
//        fifty.primesLessThan(2, 1000).forEach(System.out::println);
//        Assertions.assertThat(fifty.primesLessThan(2, 1000)).contains(41);
    }


}