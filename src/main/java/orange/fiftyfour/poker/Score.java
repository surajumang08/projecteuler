/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public class Score implements Comparable<Score>{
    private Rank rank;
    private Integer value = 0; //to be used as tie breaker in case of a tie in rank.
    private static final Score empty = new Score(Rank.EMPTY, 0);

    private Score(Rank rank, Integer value) {
        this.rank = rank;
        this.value = value;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static Score createScore(Rank rank){
        return createScore(rank, 0);
    }

    public static Score createScore(Rank rank, Integer value){
        return new Score(rank, value);
    }

    public static Score emptyScore(){
        return empty;
    }

    //positive if the current object is greater than the other.
    //zero if both are equal.
    //negetive if the current is smaller than the other.
    @Override
    public int compareTo(Score o) {
        if (o == this){
            return 0;
        }
        if (o.rank == this.rank){
            return this.value - o.value;
        }
        return this.rank.ordinal() - o.rank.ordinal();
    }
}
