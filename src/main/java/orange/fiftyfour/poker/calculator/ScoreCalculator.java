/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator;

import orange.fiftyfour.poker.Pokerhand;
import orange.fiftyfour.poker.Score;

import java.util.Arrays;
import java.util.List;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public abstract class ScoreCalculator {
    //consider setting it to Set<ScoreCalculator>.
    private Pokerhand pokerhand;

    public final Pokerhand getPokerhand() {
        return pokerhand;
    }

    public ScoreCalculator(Pokerhand pokerhand) {
        this.pokerhand = pokerhand;
    }

    public final String getMaxValuedHand(){
        return null;
    }

    public abstract Score calculate();

    public static List<Class<? extends ScoreCalculator>> getAllCalculators(){
        return Arrays.asList(FlushCalculator.class, RoyalFlushCalculator.class);
    }
}
