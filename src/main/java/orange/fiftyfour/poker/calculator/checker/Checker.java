/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator.checker;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public abstract class Checker {

    public abstract boolean check(String value);
}
