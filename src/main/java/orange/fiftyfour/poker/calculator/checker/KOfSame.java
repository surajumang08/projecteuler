/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator.checker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */
public class KOfSame extends Checker {

    private int K;

    public KOfSame(int k) {
        K = k;
    }

    // there is no distinction between suites and values here. simply checks the existence of a value in the
    // string which is present K times.
    @Override
    public boolean check(String value) {
        Map<Character, Integer> countMap = new HashMap<>();
        for (int i = 0; i < value.length(); i++) {
            countMap.merge(value.charAt(i), 1, (a,b) -> a+b);
        }
//        check if there exist a value which is greater than equals to K in the map.
        return countMap.values().stream().max(Integer::compareTo).get() >= K;
    }
}
