/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator.checker;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */

// checks whether there exist K consecutive values.
public class ConsecutiveValuesChecker extends Checker {
    int K;
    private static final String ORDER = "23456789TJQKA";


    public ConsecutiveValuesChecker(int k) {
        K = k;
    }

    //check if there are K consecutive values in the given String.
    public boolean check(String value){
        return true;
    }

    public boolean check1(String values){
        for (int i = 0; i < values.length() ; i++) {
            String start = values.substring(i,i+1);
            int index = ORDER.indexOf(start);
            if (index < 0 || i+K > values.length() || index+K > ORDER.length()){
                continue;
            }
            if (values.substring(i, i+K).equals(ORDER.substring(index, index+K))){
                return true;
            }
        }
        return false;
    }
}
