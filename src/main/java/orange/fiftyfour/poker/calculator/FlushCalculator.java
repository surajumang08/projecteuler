/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator;

import orange.fiftyfour.poker.Pokerhand;
import orange.fiftyfour.poker.Rank;
import orange.fiftyfour.poker.Score;
import orange.fiftyfour.poker.calculator.checker.Checker;
import orange.fiftyfour.poker.calculator.checker.KOfSame;

import java.util.stream.Collectors;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */

// returns a valid Score only if the Score for PokerHand is a Flush otherWise it returns the EmptyScore.
public class FlushCalculator extends ScoreCalculator {

    private static Checker checker = new KOfSame(5);

    public FlushCalculator(Pokerhand pokerhand) {
        super(pokerhand);
    }

    @Override
    public Score calculate() {
        if (! isFlush()) {
            return Score.emptyScore();
        }
        return Score.createScore(Rank.FLUSH, getHighest());
    }

    public Integer getHighest(){
        return getPokerhand().getHand().get(4).getCardValueAsInt();
    }

    // test whether all the suites are same or not.
    public boolean isFlush(){
        return isFlush(getPokerhand());
    }

    public static boolean isFlush(Pokerhand pokerhand){
        String str = pokerhand.getHand().stream().map(Pokerhand.Card::getSuite).collect(Collectors.joining());
        return checker.check(str);
    }
}
