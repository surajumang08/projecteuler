/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker.calculator;

import orange.fiftyfour.poker.Pokerhand;
import orange.fiftyfour.poker.Rank;
import orange.fiftyfour.poker.Score;
import orange.fiftyfour.poker.calculator.checker.Checker;
import orange.fiftyfour.poker.calculator.checker.KOfSame;

import java.util.stream.Collectors;

/**
 * Created 3/5/2019
 *
 * @author sjkumar
 */
public class FourOfAKind extends ScoreCalculator {

    private Checker checker = new KOfSame(4);

    public FourOfAKind(Pokerhand pokerhand) {
        super(pokerhand);
    }

    @Override
    public Score calculate() {
        if (! isValid()){
            return Score.emptyScore();
        }
        return Score.createScore(Rank.FOUR_OF_A_KIND, 22);
    }

    private boolean isValid(){
        return checker.check(getPokerhand().getHand().stream().map(Pokerhand.Card::getCardValue).collect(Collectors.joining()));
    }
}
