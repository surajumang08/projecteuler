/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker;

/**
 * Created 3/4/2019
 *
 * @author sjkumar
 */

public enum Rank {
    EMPTY,
    HIGH_CARD,  //Highest value card among five cards.
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    STRAIGHT,   //all cards are consecutive
    FLUSH,      //all cards of same suite
    FULL_HOUSE, //three of a kind and a pair.
    FOUR_OF_A_KIND,
    STRAIGHT_FLUSH,//all cards are consecutive and of the same suite.
    ROYAL_FLUSH     // consecutive cards starting with a Ten.

}
