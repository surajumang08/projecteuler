/*
 * Copyright 2006-2010 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.fiftyfour.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static orange.fiftyfour.poker.Pokerhand.Card.create;

/**
 * Created 3/5/2019
 *
 * @author sjkumar
 */
public class Pokerhand {
    private List<Card> hand = new ArrayList<>();

    public static final Pokerhand simpleHand = new Pokerhand(Arrays.asList("4H", "2S", "3C", "5D", "7H"));


    public Pokerhand(List<String> hand){
        validate(hand);
        this.hand = hand.stream().map(Card::create).sorted().collect(Collectors.toList());
    }

    private static void validate(List<String> hand) {
        if (hand.size() != 5) {
            throw new RuntimeException("Number of cards in a hand can be exactly Five only");
        }
    }

    public List<Card> getHand() {
        return hand;
    }

    // we want to be able to sort cards according to their value ignoring the suite.
    public static class Card implements Comparable<Card>{
        private String cardValue;
        private String suite;
        private static final String ORDER = "23456789TJQKA";
        private static final String SUITES = "HCSD";

        Card(String cardValue, String suite) {
            if (!ORDER.contains(cardValue) || !SUITES.contains(suite)){
                throw new RuntimeException("Invalid suit of value");
            }
            this.cardValue = cardValue;
            this.suite = suite;
        }

        static Card create(String card){
            return new Card(card.substring(0,1), card.substring(1));
        }

        public String getCardValue() {
            return cardValue;
        }

        public String getSuite() {
            return suite;
        }

        public Integer getCardValueAsInt(){
            return 2 + ORDER.indexOf(cardValue);
        }

        @Override
        public int compareTo(Card o) {
            if (o == this){
                return 0;
            }
            if (! this.cardValue.equals(o.cardValue)){
                int first = ORDER.indexOf(this.cardValue);
                int second = ORDER.indexOf(o.cardValue);
//                if (first <0 || second <0){
//                    throw new RuntimeException("Invalid Value")
//                }
                return first - second;
            }
            return 0;
        }
    }
}
