/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample.handler;

import orange.sample.Question;
import orange.sample.Response;
import orange.sample.handler.ResponseHandler;
import orange.sample.validator.ResponseValidator;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class NTriesResponseHandler extends BasicResponseHandler {
    private final Integer N;
    private Integer numberOfTries;

    public NTriesResponseHandler(ResponseValidator responseValidator, Integer numberOfTries) {
        super(responseValidator);
        N = numberOfTries;
        this.numberOfTries = numberOfTries;
    }

    @Override
    public boolean acceptResponse() {
        return numberOfTries-- > 0;
    }

    @Override
    public void handleResponse(Question question) {
        do {
            if (responseValidator.test(readUserResponse(question))) {
                System.out.println(getCorrectMessage());
                return;
            }
            System.out.println(getIncorrectMessage());
        } while (acceptResponse());
        System.out.println(getCorrectResponseMessage(question));
    }

    @Override
    public void clear() {
        numberOfTries = N;
    }
}
