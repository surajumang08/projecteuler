/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample.handler;

import orange.sample.Question;
import orange.sample.Response;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public interface ResponseHandler {
    String getCorrectResponseMessage(Question question);
    String getIncorrectMessage();
    String getCorrectMessage();
    boolean acceptResponse();
    void handleResponse(Question question);
    Response readUserResponse(Question question);
    void clear();
}
