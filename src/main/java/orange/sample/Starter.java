/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample;

import orange.sample.handler.NTriesResponseHandler;
import orange.sample.validator.AdditionValidator;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */

/*
* The requirement is to generate random numbers based on `# of digits`
* To be able to support arithmetic operators {+,-,*,/}
* Another option to turn on the persistent feature, which will not proceed ahead until correct answer is
* entered.
* Recording time for each response.
* Sequence of args, 1) #of digits 2) #of digits 3) operator 4)--persist(optional)
* */
public class Starter {

    public static void main(String[] args) {
        Integer digitLeft = Integer.parseInt(args[0]);
        Integer digitRight = Integer.parseInt(args[1]);
        String operator = args[2];

        Question.Operator operator1 = Question.Operator.valueOf(operator);
        MainController mainController = new MainController.MainControllerBuilder()
                .setLeftDigits(digitLeft)
                .setRightDigits(digitRight)
                .setOperator(operator1)
                .setResponseHandler(new NTriesResponseHandler(operator1.getResponseValidator(), 0))
                .build();
        mainController.run();
    }
}
