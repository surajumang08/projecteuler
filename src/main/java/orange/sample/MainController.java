/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample;

import orange.sample.handler.ResponseHandler;
import orange.sample.handler.NTriesResponseHandler;
import orange.sample.validator.AdditionValidator;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class MainController {

    private QuestionGenerator questionGenerator;
    private ResponseHandler responseHandler;

    public MainController(QuestionGenerator questionGenerator, ResponseHandler responseHandler) {
        this.questionGenerator = questionGenerator;
        this.responseHandler = responseHandler;
    }

    public void run(){
        for (int i = 0; i < 5; i++) {
            Question question = questionGenerator.getNext();
            System.out.println(question.getPrettyPrinted());
            responseHandler.handleResponse(question);
            responseHandler.clear();
        }
    }

    public static class MainControllerBuilder {
        private Integer leftDigits = 2;
        private Integer rightDigits = 2;
        private Question.Operator operator = Question.Operator.SUM;
        private ResponseHandler responseHandler =
                new NTriesResponseHandler(operator.getResponseValidator(), 2);

        public MainControllerBuilder setLeftDigits(Integer leftDigits){
            this.leftDigits = leftDigits;
            return this;
        }

        public MainControllerBuilder setRightDigits(Integer rightDigits) {
            this.rightDigits = rightDigits;
            return this;
        }

        public MainControllerBuilder setOperator(Question.Operator operator) {
            this.operator = operator;
            return this;
        }

        public MainControllerBuilder setResponseHandler(ResponseHandler responseHandler) {
            this.responseHandler = responseHandler;
            return this;
        }

        public MainController build (){
            QuestionGenerator questionGenerator = QuestionGenerator.create(leftDigits, rightDigits, operator);
            return new MainController(questionGenerator, responseHandler);
        }

    }

}
