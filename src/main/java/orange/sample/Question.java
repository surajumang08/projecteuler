/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample;

import orange.sample.validator.*;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class Question {

    private Integer left;
    private Integer right;
    private Operator operator;

    public enum Operator{
        SUM("+", new AdditionValidator()),
        PRODUCT("*", new ProductValidator()),
        DIVISION("/", new DivisionValidator()),
        DIFFERENCE("-", new DifferenceValidator());

        private String symbol;
        private ResponseValidator responseValidator;

        Operator(String value, ResponseValidator validator) {
            symbol = value;
            responseValidator = validator;
        }

        public String getSymbol(){
            return " " + symbol + " ";
        }

        public ResponseValidator getResponseValidator(){
            return responseValidator;
        }
    }

    public Question(Integer left, Integer right, Operator operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    public static Question create(Integer left, Integer right, Operator operator){
        return new Question(left, right, operator);
    }

    public Integer getLeft() {
        return left;
    }

    public Integer getRight() {
        return right;
    }

    public Operator getOperator() {
        return operator;
    }

    public String getPrettyPrinted(){
        return "What is " + left + operator.getSymbol() + right;
    }
}
