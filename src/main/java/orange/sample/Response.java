/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class Response {
    private Question question;
    private Integer response;

    public Response(Question question, Integer response) {
        this.question = question;
        this.response = response;
    }

    public static Response create(Question question, Integer response){
        return new Response(question, response);
    }

    public Question getQuestion() {
        return question;
    }

    public Integer getResponse() {
        return response;
    }
}
