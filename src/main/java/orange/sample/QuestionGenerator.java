/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample;


import orange.sample.Question.Operator;

import java.util.Random;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class QuestionGenerator {
    private Integer digitsLeft;
    private Integer digitsRight;
    private Operator operator;

    private static final Random random = new Random();

    public QuestionGenerator(Integer digitsLeft, Integer digitsRight, Operator operator) {
        this.digitsLeft = digitsLeft;
        this.digitsRight = digitsRight;
        this.operator = operator;
    }

    public static QuestionGenerator create(Integer digitsLeft, Integer digitsRight, Operator operator){
        return new QuestionGenerator(digitsLeft, digitsRight, operator);
    }

    public Question getNext(){
        return new Question(getRandomInteger(digitsLeft), getRandomInteger(digitsRight), operator);
    }

    private static Integer getRandomInteger(Integer digits){
        int mod = (int) Math.pow(10, digits);
        return Math.abs(random.nextInt() % mod);
    }
}
