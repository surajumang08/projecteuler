/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.sample.validator;

import orange.sample.Question;
import orange.sample.Response;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public class DivisionValidator implements ResponseValidator {
    @Override
    public boolean test(Response response) {
        Question question = response.getQuestion();
        if (question.getOperator() != Question.Operator.DIVISION){
            throw new RuntimeException("Only sum can be validated by AdditionValidator");
        }
        Integer actualResponse = getCorrectResponse(response.getQuestion());
        return actualResponse.equals(response.getResponse());
    }

    @Override
    public Integer getCorrectResponse(Question question) {
        return question.getLeft() / question.getRight();
    }
}
