package orange.fortysix;


import org.apache.commons.math3.primes.Primes;

import java.util.stream.Stream;

/*
find a example to counter the Goldbach's conjecture.
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + pow(1, 2) * 2
15 = 7 + pow(2, 2) * 2
21 = 3 + pow(3, 2) * 2
25 = 7 + pow(3, 2) * 2
27 = 19 + pow(2, 2) * 2
33 = 31 + pow(1, 2) * 2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
 */
/*
    1) Need to find odd numbers which are not prime. (straight forward)
    2) for every odd composite try to find a prime and a square number. (Stop if you couldn't find one)
    3) Find a way to resolve an odd consecutive number into a prime and a twice of a square.
    4) The prime is going to make it easier. For a odd Consecutive N, try out all the prime numbers smaller than
        N and see if after subtraction you get twice of a square.

        Formally
        for all primes p < N do:
            if sqrt((N-p)/2) is an integer
                pick another N and repeat.
        if the loop finishes then the last number which the loop used is our result.
 */
public class FortySix {

    int seed = 3;

    public int loop(int N){
//        System.out.println("Checking For " + N);
        int prime = Primes.nextPrime(1);
        for ( ; prime < N; prime = Primes.nextPrime(prime + 1)){
//            System.out.println("trying " + prime);
            if (!isEven(N-prime)){
                continue;
            }
            int difference = (N-prime)/2;
            // match found for N
            if (isWhole(Math.sqrt(difference))){
                return -1;
            }
        }
        System.out.println("FOund a number : " + N);
        return N;
    }

    public boolean isEven(int num){
        return num % 2 == 0;
    }

    public boolean isWhole(double num){
        return (num - Math.floor(num)) == 0;
    }

    public void execute(int limit){
        Stream.generate(() -> seed += 2)
                .filter(num -> !Primes.isPrime(num))
                .limit(limit)
                .filter(num -> loop(num) > 0)
                .forEach(System.out::println);
    }
}
