package orange.fifty;

/*
For solving projecteuler problem number 50.
We are required to find the prime number less than a million which is sum of N consecutive prime numbers
where N is as large as possible.

The prime 41, can be written as the sum of six consecutive primes:
41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
Various approaches including the brute-force one needs to be tried.

Breaking the problem down to
        1) given a prime number, tell whether it can be written as a sum of consecutive primes or not?
        2) given a prime number that can be represented as a sum of consecutive primes, list the primes which add
            up to the given number.
      # 3) given a number N, find a sequence of N consecutive primes such that they sum up to another Prime such
            less that the min prime in the sequence is less than K.
        4) given a Prime number N, find out if there is a sequence of consecutive primes such that they sum to
            another prime.
 */

import org.apache.commons.math3.primes.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fifty {

    /*
    Subtask 4: a) Store all the prime numbers upto 1000.
               b) Create a Map of Long to Long to store the max length of sequences stating with Key.
               c) For each prime less than 1000, find out the max sequence which gives a sum of Prime.
     */
    public static List<Integer> primes = new ArrayList<>();
    public static Map<Integer, Integer> SEQUENCE_LENGTH = new TreeMap<>();
    private int seed;

    public void execute() {
        addPrimes(100000);
        primes.forEach(prime-> primesLessThan(prime, 1000000));
//        SEQUENCE_LENGTH.entrySet().forEach();
        List<Map.Entry<Integer, Integer>> sortedEntries = new ArrayList<>(SEQUENCE_LENGTH.entrySet());
        sortedEntries.sort(Map.Entry.comparingByValue());
        sortedEntries.forEach(entry -> System.out.println(entry.getKey() + " "+ entry.getValue()));
    }

    public void addPrimes(int quantity){
        seed = 0;
        primes = Stream.generate(() -> seed++)
                .filter(Primes::isPrime)
                .limit(quantity)
                .collect(Collectors.toList());
    }

    public void primesLessThan(int start, int limit){
        List<Integer> result = new ArrayList<>();
        int sum = 0;
        int nextPrime = start;
//        System.out.print("Printing a sequence ");
//        System.out.println("All primes starting with " + start);
        int count = 0;

        while (sum <= limit){
            sum += nextPrime;
            if (Primes.isPrime(sum) && count > 0){
                addSequence(sum, count);
//                System.out.println(sum + " " + count);
            }
//            System.out.print(" "+ nextPrime);
            count++;
            nextPrime = Primes.nextPrime(nextPrime+1);
        }
    }

    private void addSequence(int prime, int sequenceLength){
        if (SEQUENCE_LENGTH.get(prime) != null){
            if (SEQUENCE_LENGTH.get(prime) < sequenceLength){
                SEQUENCE_LENGTH.put(prime, sequenceLength);
            }
            return;
        }
        SEQUENCE_LENGTH.put(prime, sequenceLength);
    }
}
